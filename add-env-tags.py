import boto3

ec2_client_sao = boto3.client('ec2', region_name="sa-east-1")
ec2_resource_sao = boto3.resource('ec2', region_name="sa-east-1")
instance_ids_sao = []

ec2_client_virginia = boto3.client('ec2', region_name="us-east-1")
ec2_resource_virginia = boto3.resource('ec2', region_name="us-east-1")
instance_ids_virginia = []

reservations_sao = ec2_client_sao.describe_instances()['Reservations']
for res in reservations_sao:
    instances = res['Instances']
    for ins in instances:
        instance_ids_sao.append(ins["InstanceId"])

reservations_virginia = ec2_client_virginia.describe_instances()['Reservations']
for res in reservations_virginia:
    instances = res['Instances']
    for ins in instances:
        instance_ids_virginia.append(ins["InstanceId"])

response_sao = ec2_resource_sao.create_tags(
    Resources = instance_ids_sao,
    Tags = [
        {
            'Key': 'env',
            'Value': 'prod'
        }
    ]
)

response_virginia = ec2_resource_virginia.create_tags(
    Resources = instance_ids_virginia,
    Tags = [
        {
            'Key': 'env',
            'Value': 'devel'
        }
    ]
)