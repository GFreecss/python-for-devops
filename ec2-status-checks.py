import boto3
import schedule


ec2_client = boto3.client('ec2', region_name="sa-east-1")
ec2_resource = boto3.resource('ec2', region_name="sa-east-1")

def check_instance_status():
    # Print the Status and Status Checks of EC2 Instances
    instances_status = ec2_client.describe_instance_status()
    for i_status in instances_status["InstanceStatuses"]:
        ins_status = i_status['InstanceStatus']['Status']
        sys_status = i_status['SystemStatus']['Status']
        state = i_status["InstanceState"]["Name"]

        print(f"Instance {i_status['InstanceId']} instance status is {ins_status}, system status is {sys_status}.\nActually the instance is {state}")
    print("\n#################################")

schedule.every(5).seconds.do(check_instance_status)

while True:
    schedule.run_pending()

########################################################################################
# instances = ec2_client.describe_instances()

# # Print the State of EC2 instances.
# for reservation in instances["Reservations"]:
#     for instance in reservation["Instances"]:
#         print(f"Instance {instance['InstanceId']} is {instance['State']['Name']}")